# Welcome
これは大学の友人達にゲームプログラミングを教えるために作ったサンプルソースです。

# ビルド方法
1. リポジトリをダウンロード  
https://bitbucket.org/HY_RORRE/dxlibtest/downloads/

1. DXライブラリをここからダウンロード  
http://dxlib.o.oo7.jp/dxdload.html

1. "プロジェクトに追加すべきファイル_VC用"フォルダを"DxLib"にリネーム

1. プロジェクトフォルダにコピー

1. "DxLibTest.sln"を開いてビルド、実行