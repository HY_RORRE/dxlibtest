#include "DxLib.h"
#include <math.h>

// 自機の構造体定義
struct Character{
	int x;
	int y;

	int v;

	int w;
	int h;

	int r; // 衝突判定の半径

	int handle;
};

// 自機の変数宣言
Character chara = {100, 300, 4, 0, 0, 3, 0};

void DrawCharacter(){
	DrawGraph(chara.x, chara.y, chara.handle, TRUE); // 自機画像を描画

	int center_x = chara.x + chara.w / 2; // 自機の中心座標を計算
	int center_y = chara.y + chara.h / 2; // 自機の中心座標を計算
	DrawCircle(center_x, center_y, chara.r, GetColor(0, 255, 0)); // 自機の判定範囲を緑の円で塗りつぶす
}

// 弾の構造体定義
struct Bullet{
	int x;
	int y;
	int vx;
	int vy;

	int w;
	int h;

	int r; // 衝突判定の半径

	int handle;

	int display_flag;
};

// 弾の最大個数
#define MAX_BULLET 100

// 弾の変数宣言
Bullet bullet[MAX_BULLET] = {
	{0, 0, 1, 1, 0, 0, 20, 0, 1},
	{10, 10, 1, 0, 0, 0, 20, 0, 1},
	{10, 20, 0, 1, 0, 0, 20, 0, 1}
};

// 弾の座標を計算する関数。1フレームに1回だけ呼ぶ。
void UpdateBullet(){
	for(int i = 0; i < MAX_BULLET; i++){
		bullet[i].x += bullet[i].vx;
		bullet[i].y += bullet[i].vy;
	}
}

// 衝突判定関数 (1のとき自機と弾が衝突)
int NotareJinnderu(){
	int chara_x0 = chara.x + chara.w / 2, chara_y0 = chara.y + chara.h / 2;
	for(int i = 0; i < MAX_BULLET; i++){
		if(bullet[i].display_flag == 1){
			int bullet_x0 = bullet[i].x + bullet[i].w / 2;
			int bullet_y0 = bullet[i].y + bullet[i].h / 2;

			int distance_x = chara_x0 - bullet_x0;
			int distance_y = chara_y0 - bullet_y0;

			int crush_distance = chara.r + bullet[i].r;

			if(distance_x * distance_x + distance_y * distance_y <= crush_distance * crush_distance){
				return 1;
			}
		}
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
	ChangeWindowMode(TRUE);  // フルスクリーン→ウィンドウモードに変更
	SetWindowText("ないふぅ殺すぞ"); // ウィンドウタイトル変更
	if(DxLib_Init() == -1){  // ＤＸライブラリ初期化処理
		return -1;           // エラーが起きたら直ちに終了
	}
	SetDrawScreen(DX_SCREEN_BACK); // 裏画面描画モードに変更

	chara.handle = LoadGraph("画像/キャラクタ01.png"); // 自機画像をロード
	GetGraphSize(chara.handle, &chara.w, &chara.h); // 自機画像の幅と高さを取得

	int bullet_handle = LoadGraph("画像/弾00.png"); // 弾の画像をロード
	int bullet_w, bullet_h;

	GetGraphSize(bullet_handle, &bullet_w, &bullet_h); // 弾の幅と高さを取得

	for(int i = 0; i < MAX_BULLET; i++){ // 取得したハンドル、幅、高さをすべての弾の変数に代入していく
		bullet[i].w = bullet_w;
		bullet[i].h = bullet_h;
		bullet[i].handle = bullet_handle;
	}

	int x = 0, y = 0; // 座標変数の宣言
	while(ProcessMessage() == 0){ // 閉じるボタンが押されたらループから抜ける

		chara.v = 4; // 移動速度
					 // キー入力確認 -> 座標変更
		if(CheckHitKey(KEY_INPUT_SPACE) == 1) chara.v = 2; // スペース押してる間は移動速度を遅くする
		if(CheckHitKey(KEY_INPUT_RIGHT) == 1) chara.x += chara.v;
		if(CheckHitKey(KEY_INPUT_LEFT) == 1) chara.x -= chara.v;
		if(CheckHitKey(KEY_INPUT_DOWN) == 1) chara.y += chara.v;
		if(CheckHitKey(KEY_INPUT_UP) == 1) chara.y -= chara.v;

		DrawCharacter(); // 自機画像を描画

		UpdateBullet();
		for(int i = 0; i < MAX_BULLET; i++){
			if(bullet[i].display_flag == 1){
				DrawGraph(bullet[i].x, bullet[i].y, bullet[i].handle, TRUE); // 弾の描画
			}
		}

		if(NotareJinnderu() == 1){
			for(int i = 0; i < MAX_BULLET; i++){
				bullet[i].display_flag = 0;
			}
		}

		ScreenFlip();      // 裏画面を表画面に表示
		ClearDrawScreen(); // 裏画面を黒でクリア
	}

	DxLib_End(); // ＤＸライブラリ使用の終了処理
	return 0;    // ソフトの終了
}